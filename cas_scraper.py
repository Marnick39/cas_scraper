from lxml import html
import requests
import re
print("Enter the chemicals you want to search for with spaces between them..")
s = input()
terms = list(map(str, s.split()))
print("Confirm?")
input()
fails = []
for term in terms:
  url =  'https://www.sigmaaldrich.com/catalog/search?term={term}&interface=All&N=0&mode=match partialmax&lang=en&region=BE&focus=product'.format(term=term)

  page = requests.get(url)
  tree = html.fromstring(page.content)

  stuff = tree.xpath('/html/body/div[3]/div/div/div[2]/div/div[7]/div/div[2]/div[1]/div/div[2]/ul/li/ul/li[3]/p/span/a')

  if len(stuff) > 0: # found chemical!
    html_result = html.tostring(stuff[0]).decode('utf-8')
    regex_result = re.search('[0-9]{1,7}-{1}[0-9]{2}-[0-9]{1}', html_result)
    print(term + ', cas: ' + regex_result.group(0))
  
  else: # failed to find chemical, add to fails
    fails.append(term)
if len(fails) > 0:
  print("---------------")

  print("Couldn't find the following things: (" + str(len(fails)) + "/" + str(len(terms)) + ")")
  for f in fails:
      print(''.join(map(str, f)))
